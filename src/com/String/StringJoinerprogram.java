package com.String;

import java.util.StringJoiner;

public class StringJoinerprogram {

	public static void main(String[] args) {
           StringJoiner str=new StringJoiner(",","[","]");
           str.add("Ajay").add("Balaji").add("Christoper").add("Dhanush");
           System.out.println(str);
           
           StringJoiner str1=new StringJoiner(":");
           str1.add("Welcome To Globallogic ");
           
           str1.merge(str);
           System.out.println(str1);

           
	}

}
